/* 
1) How do you create arrays in JS?
2) How do you access the first character of an array?
3) How do you access the last character of an array?
4) What array method searches for, and returns the index of a given value in an array? This method returns -1 if given value is not found in the array.
5) What array method loops over all elements of an array, performing a user-defined function on each iteration?
6) What array method creates a new array with elements obtained from a user-defined function?
7) What array method checks if all its elements satisfy a given condition?
8) What array method checks if at least one of its elements satisfies a given condition?
9) True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
10) True or False: array.slice() copies elements from original array and returns them as a new array.
*/

/* 
Answers:
1. const arrayName = [];
2. arrayName[0]
3. arrayName[arrayName.length - 1];
4. indexOf()
5. forEach()
6. map()
7. every()
8. some()
9. False
10. True
*/

/* 
Function Coding

1) Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
 */

const students = ["John", "Joe", "Jane", "Jessie"];

const addToEnd = (arrayName, name) => {
  if (typeof name !== "string") {
    console.log("error - can only add strings to an array");
  } else {
    arrayName.push(name);
    console.log(arrayName);
  }
};

//Output
addToEnd(students, "Ryan"); //["John", "Joe", "Jane", "Jessie", "Ryan"]
addToEnd(students, 045); //"error - can only add strings to an array"

/* 
2) Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
*/
const addToStart = (arrayName, name) => {
  if (typeof name !== "string") {
    console.log("error - can only add strings to an array");
  } else {
    arrayName.unshift(name);
    console.log(arrayName);
  }
};

//Output
addToStart(students, "Tess"); //["Tess", "John", "Joe", "Jane", "Jessie", "Ryan"]
// //validation check
addToStart(students, 033); //"error - can only add strings to an array"

/*
3) Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.

*/
const elementChecker = (arrayName, name) => {
  if (arrayName.length <= 0) {
    console.log("error - passed in array is empty");
  } else {
    if (arrayName.includes(name)) {
      console.log(true);
    }
  }
};

//test input
elementChecker(students, "Jane"); //true
// //validation check
elementChecker([], "Jane"); //"error - passed in array is empty"

/*
4) Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:


if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.

*/

const checkAllStringsEnding = (arrayName, stringToFind) => {
  function isNotString(item) {
    return typeof item != "string";
  }
  function endsWith(item) {
    return item.endsWith(stringToFind);
  }

  if (arrayName.length <= 0) {
    console.log("error - array must NOT be empty");
  } else {
    if (arrayName.some(isNotString)) {
      console.log("error - all array elements must be strings");
    } else {
      if (typeof stringToFind != "string") {
        console.log(" error -2nd argument must be of data type string");
      } else {
        if (stringToFind.length > 1) {
          console.log("error - 2nd argument must be a single character");
        } else {
          if (arrayName.every(endsWith)) {
            console.log(true);
          } else {
            console.log(false);
          }
        }
      }
    }
  }
};

//test input
checkAllStringsEnding(students, "e"); //false
//validation checks
checkAllStringsEnding([], "e"); //"error - array must NOT be empty"
checkAllStringsEnding(["Jane", 02], "e"); //"error - all array elements must be strings"
checkAllStringsEnding(students, 4); //"error - 2nd argument must be of data type string"
checkAllStringsEnding(students, "el"); //"error - 2nd argument must be a single character"

/*
5) Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
*/

const stringLengthSorter = (arrayList) => {
  function isNotString(item) {
    return typeof item != "string";
  }

  if (arrayList.some(isNotString)) {
    console.log("error - all array elements must be strings");
  } else {
    let newArray = arrayList.sort((a, b) => {
      return a.length - b.length;
    });
    console.log(newArray);
  }
};

//test input
stringLengthSorter(students); //["Joe", "Tess", "John", "Jane", "Ryan", "Jessie"]
//validation check
stringLengthSorter([037, "John", 039, "Jane"]); //"error - all array elements must be strings"

/*
6) Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:

if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
return the number of elements in the array that start with the character argument, must be case-insensitive

Use the students array and the character "J" as arguments when testing.
*/
const startsWithCounter = (arrayName, stringToFind) => {
  const containsString = [];
  function isNotString(item) {
    return typeof item != "string";
  }

  if (arrayName.length <= 0) {
    console.log("error - array must NOT be empty");
  } else {
    if (arrayName.some(isNotString)) {
      console.log("error - all array elements must be strings");
    } else {
      if (typeof stringToFind != "string") {
        console.log("error - 2nd argument must be of data type string");
      } else {
        if (stringToFind.length > 1) {
          console.log("error - 2nd argument must be a single character");
        } else {
          arrayName.filter((item) => {
            if (item.toLowerCase().startsWith(stringToFind)) {
              containsString.push(item);
            }
          });
          console.log(containsString.length);
        }
      }
    }
  }
};

//test input
startsWithCounter(students, "j"); //4

/*
7) Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:



if array is empty, return "error - array must NOT be empty"
if at least one array element is NOT a string, return "error - all array elements must be strings"
if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

Use the students array and the string "jo" as arguments when testing.

*/

const likeFinder = (arrayName, stringToFind) => {
  const containsString = [];
  function isNotString(item) {
    return typeof item != "string";
  }

  if (arrayName.length <= 0) {
    console.log("error - Array must NOT be empty");
  } else {
    if (arrayName.some(isNotString)) {
      console.log("error - all array elements must be strings");
    } else {
      if (typeof stringToFind != "string") {
        console.log("error - argument must be of data type string");
      } else {
        arrayName.forEach((item) => {
          if (item.toLowerCase().includes(stringToFind.toLowerCase())) {
            containsString.push(item);
          }
        });
        console.log(containsString);
      }
    }
  }
};

//test input
likeFinder(students, "jo"); //["Joe", "John"]

/*
8) Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
*/
const randomPicker = (arrayName) => {
  const min = 0;
  const max = arrayName.length - 1;
  const randomNumber = Math.floor(Math.random() * (max - min)) + min;
  console.log(arrayName[randomNumber]);
};

/*
//test input
*/
randomPicker(students); //"Ryan"
randomPicker(students); //"John"
randomPicker(students); //"Jessie"
